import ActorSheetConan2d20 from './base';

class ActorSheetConan2d20NPC extends ActorSheetConan2d20 {
  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      classes: options.classes.concat([
        'conan2d20',
        'actor',
        'npc',
        'character-sheet',
      ]),
      width: 550,
      height: 690,
      resizable: false,
      tabs: [
        {
          navSelector: '.npc-sheet-navigation',
          contentSelector: '.npc-sheet__body',
          initial: 'npc-details',
        },
      ],
      dragDrop: [{dragSelector: '.item', dropSelector: null}],
    });
    return options;
  }

  get template() {
    const path = 'systems/conan2d20/templates/actors/';
    if (!game.user.isGM && this.actor.limited)
      return `${path}readonly-npc-sheet.html`;
    return `${path}npc-sheet.html`;
  }

  activateListeners(html) {
    html.find('.isMob-checkbox').click(event => this._onToggleMob(event));

    html.find('.npc-mob-count-minus')
      .click(event => this._onAlterMobCount(event, -1));

    html.find('.npc-mob-count-plus')
      .click(event => this._onAlterMobCount(event, 1));

    const me = this;
    html.find('.mob-wound').each(function () {
      const wound = $(this);
      const mobMemberId = wound.data('mob-member-id');
      const woundIndex = wound.data('wound-index');

      wound.click(ev => {
        ev.preventDefault();
        me._onToggleWound(mobMemberId, woundIndex);
      });
    });

    html.find('.mob-trauma').each(function () {
      const wound = $(this);
      const mobMemberId = wound.data('mob-member-id');
      const traumaIndex = wound.data('trauma-index');

      wound.click(ev => {
        ev.preventDefault();
        me._onToggleTrauma(mobMemberId, traumaIndex);
      });
    });

    html.find('.quantity-grid').each(function () {
      const spinner = $(this);
      const mobMemberId = spinner.data('mob-member-id');

      const input = spinner.find('input[type="number"]');
      const btnUp = spinner.find('.quantity-up');
      const btnDown = spinner.find('.quantity-down');
      const quantityType = input.attr('data-quantity-type');

      input.on('wheel', ev => {
        ev.preventDefault();
        if (ev.originalEvent.deltaY < 0) {
          me[`_mobInc${quantityType}`](mobMemberId);
        } else if (ev.originalEvent.deltaY > 0) {
          me[`_mobDec${quantityType}`](mobMemberId);
        }
      });

      btnUp.click(ev => {
        ev.preventDefault();
        me[`_mobInc${quantityType}`](mobMemberId);
      });

      btnDown.click(ev => {
        ev.preventDefault();
        me[`_mobDec${quantityType}`](mobMemberId);
      });
    });

    this._mobMemberContextMenu(html);

    super.activateListeners(html);
  }

  async getData(options = {}) {
    const context = await super.getData(options);
    context.flags = context.actor.flags;

    // Update expertise fields labels
    if (context.system.skills !== undefined) {
      for (const [s, skl] of Object.entries(context.system.skills)) {
        skl.label = CONFIG.CONAN.expertiseFields[s];
      }
    }

    // Remove dead fields if they exist
    delete context.system.isMinion;
    delete context.system.isNemesis;
    delete context.system.isToughened;

    context.npcTypes = CONFIG.CONAN.npcTypes;
    context.npcTraits = CONFIG.CONAN.npcTraits;

    context.skills = CONFIG.CONAN.expertiseFields;

    // Create list of NPC traits, including custom ones.
    //
    let customTraits = context.actor.system.categories.custom || '';

    // Custom traits can be separated by semicolon characters to allow for
    // more than one.
    let allTraits = customTraits.split(';');
    allTraits = allTraits.filter(e => e); // filter out empty strings

    const actorTraits = this.actor.system.categories.value;

    /* eslint-disable no-unused-vars */
    actorTraits.forEach((trait, index) => {
      /* eslint-enable no-unused-vars */
      if (trait !== 'custom') {
        allTraits.push(trait);
      }
    });

    context.traits = allTraits.sort();

    context.noMobMemberId = !this.actor.system.grouping.baseActorId;

    return context;
  }

  async _clearMob() {
    const data = {
      'system.grouping.memberName': null,
      'system.grouping.members': [],
      'system.grouping.baseActorId': null,
      'system.grouping.memberImage': null,
      'system.grouping.type': null,
      'system.mobCount': 0,
      'system.isMob': false,
    };

    this.actor.update(data);
  }

  _createMobMemberData(mobActorId) {
    const mobActor = game.actors.get(mobActorId);
    const healthData = duplicate(mobActor.system.health);

    let wounds = [];
    let traumas = [];

    // NPCs have the same amount of physical and mental wounds they can take
    for (let i = 0; i < healthData.physical.wounds.max; i++) {
      wounds.push({active: false});
      traumas.push({active: false});
    }

    const data = {
      armor: mobActor.system.armor,
      courage: mobActor.system.health.courage,
      dead: false,
      resolve: {
        value: healthData.mental.max,
        max: healthData.mental.max,
        traumas,
      },
      vigor: {
        value: healthData.physical.max,
        max: healthData.physical.max,
        wounds,
      },
    };

    return data;
  }

  _getMobMemberContextOptions() {
    return [
      {
        name: game.i18n.localize('CONAN.deleteMobMemberTitle'),
        icon: '<i class="fas fa-trash"></i>',
        // condition: () => canEdit(),
        condition: () => {return game.user.isGM},
        callback: element => {
          const mobMemberDelete = element.data('mob-member-id');
          this._onMobMemberDelete(mobMemberDelete);
        },
      },
    ];
  }

  async _mobDecResolve(mobMemberId) {
    const healthData = this.actor.system.grouping.members;
    const currentResolve = healthData[mobMemberId].resolve.value;

    if (currentResolve > 0) {
      healthData[mobMemberId].resolve.value = currentResolve - 1;

      this.actor.update({
        'system.grouping.members': healthData,
      });
    }
  }

  async _mobDecVigor(mobMemberId) {
    const healthData = this.actor.system.grouping.members;
    const currentVigor = healthData[mobMemberId].vigor.value;

    if (currentVigor > 0) {
      healthData[mobMemberId].vigor.value = currentVigor - 1;

      this.actor.update({
        'system.grouping.members': healthData,
      });
    }
  }

  async _mobIncResolve(mobMemberId) {
    const healthData = this.actor.system.grouping.members;
    const currentResolve = healthData[mobMemberId].resolve.value;

    if (currentResolve < healthData[mobMemberId].resolve.max) {
      healthData[mobMemberId].resolve.value = currentResolve + 1;

      this.actor.update({
        'system.grouping.members': healthData,
      });
    }
  }

  async _mobIncVigor(mobMemberId) {
    const healthData = this.actor.system.grouping.members;
    const currentVigor = healthData[mobMemberId].vigor.value;

    if (currentVigor < healthData[mobMemberId].vigor.max) {
      healthData[mobMemberId].vigor.value = currentVigor + 1;

      this.actor.update({
        'system.grouping.members': healthData,
      });
    }
  }

  _mobMemberContextMenu(html) {
    ContextMenu.create(this, html, '.npc-mob-member', this._getMobMemberContextOptions());
  }

  async _onAlterMobCount(event, value) {
    event.preventDefault();

    if (!this.actor.system.grouping.baseActorId) {
      return ui.notifications.error(
        game.i18n.localize('CONAN.NoBaseNpcGroupMemberSet')
      );
    }

    const memberCount = this.actor.system.grouping.members.length;
    const newMemberCount = memberCount + value;

    if (newMemberCount <= 4 && newMemberCount >= 0) {
      const data = {
        'system.mobCount': newMemberCount,
      };

      let newMembers = [];

      if (newMemberCount > memberCount) {
        newMembers = this.actor.system.grouping.members;
        newMembers.push(this._createMobMemberData(this.actor.system.grouping.baseActorId));
      } else if (newMemberCount < memberCount) {
        if (newMemberCount > 0) {
          newMembers = this.actor.system.grouping.members.slice(0, newMemberCount);
        }
      }

      data['system.grouping.members'] = newMembers;

      this.actor.update(data);
    }
  }

  async _onDropItem(event, data) {
    const droppedItem = data?.uuid ? await fromUuid(data.uuid) : null;
    const type = droppedItem.type;

    if (!['npcattack', 'npcaction', 'weapon'].includes(type)) return;

    if (type === 'weapon') {
      if (droppedItem.system.weaponType === "threaten") return; // only create physical weapons

      let damageModifier = 0;
      if (droppedItem.system.weaponType === "melee") {
        damageModifier = this.actor._attributeBonus('bra');
      }
      else if (droppedItem.system.weaponType === "ranged") {
        damageModifier = this.actor._attributeBonus('awa');
      }

      const newItemData = {
        type: 'npcattack',
        name: droppedItem.name,
        img: droppedItem.img,
        system: {
          attackType: droppedItem.system.weaponType,
          damage: droppedItem.system.damage,
          description: droppedItem.system.description,
          group: droppedItem.system.group,
          qualities: droppedItem.system.qualities,
          range: droppedItem.system.range,
          size: droppedItem.system.size,
        },
      }

      // NPC attacks have the damage modifier built in to the weapon's base
      // damage
      newItemData.system.damage.dice
        = newItemData.system.damage.dice + damageModifier;

      return this.actor.createEmbeddedDocuments('Item', [newItemData]);
    }
    else {
      super._onDropItem(event, data);
    }
  }

  async _onDropActor(event, data) {
    // Only allow drops onto a Mob leader
    if (!this.actor.system.isMob) return;

    const droppedActor = data?.uuid ? await fromUuid(data.uuid) : null;

    // Only NPCs can be dropped
    if (droppedActor.type !== 'npc') return;

    if (droppedActor) {
      this._populateMob(droppedActor.id);
    }
  }

  async _onMobMemberDelete(mobMemberId) {
    const mobMembers = this.actor.system.grouping.members;
    const newMobMembers = [];

    for (let i = 0; i < mobMembers.length; i++) {
      if (i === mobMemberId) continue;

      newMobMembers.push(mobMembers[i]);
    }

    await this.actor.update({
      'system.grouping.members': newMobMembers,
      'system.mobCount': newMobMembers.length,
    });
  }

  async _onToggleMob(event) {
    const input = $(event.currentTarget);
    const enabled = input.is(':checked');

    if (enabled) {
      await this._populateMob();
    } else {
      await this._clearMob();
    }
  }

  async _onToggleWound(mobMemberId, woundIndex) {
    const mobMembers = this.actor.system.grouping.members;
    const woundStatus = mobMembers[mobMemberId].vigor.wounds[woundIndex].active;

    mobMembers[mobMemberId].vigor.wounds[woundIndex].active = !woundStatus;

    mobMembers[mobMemberId].dead = this.actor.isMobMemberDead(mobMembers[mobMemberId]);

    await this.actor.update({
      'system.grouping.members': mobMembers,
    });
  }

  async _onToggleTrauma(mobMemberId, traumaIndex) {
    const mobMembers = this.actor.system.grouping.members;
    const woundStatus = mobMembers[mobMemberId].resolve.traumas[traumaIndex].active;

    mobMembers[mobMemberId].resolve.traumas[traumaIndex].active = !woundStatus;

    mobMembers[mobMemberId].dead = this.actor.isMobMemberDead(mobMembers[mobMemberId]);

    await this.actor.update({
      'system.grouping.members': mobMembers,
    });
  }

  async _populateMob(mobActorId = null) {
    const baseActorType = this.actor.system.type;
    const baseActorId = this.actor._id;
    const baseActorIsMinion = baseActorType === 'minion';

    const groupType = baseActorIsMinion ? 'mob' : 'squad';
    const mobActor = mobActorId ? game.actors.get(mobActorId) : this.actor;
    const mobActorIsMinion = mobActor.system.type === 'minion';

    const allowOverpowered = game.settings.get(
      'conan2d20',
      'allowOverpoweredNpcGroups'
    );

    let populateAllowed = false;
    if (mobActorId) {
      if (!(allowOverpowered || mobActorIsMinion)) {
        return ui.notifications.error(
          game.i18n.localize('CONAN.NpcGroupMemberNotMinion')
        );
      } else {
        populateAllowed = true;
      }
    } else if (baseActorIsMinion) {
      populateAllowed = true;
    }

    if (populateAllowed) {
      const members = [];

      for (let i = 0; i < CONFIG.CONAN.DEFAULT_MOB_SIZE - 1; i++) {
        members.push(this._createMobMemberData(mobActorId ?? baseActorId));
      }

      const data = {
        'system.grouping.memberName': mobActor.name,
        'system.grouping.members': members,
        'system.grouping.baseActorId': mobActorId ?? baseActorId,
        'system.grouping.memberImage': mobActor.img,
        'system.grouping.type': groupType,
        'system.isMob': true,
        'system.mobCount': members.length,
      };

      this.actor.update(data);
    }
  }

  _prepareItems(context) {
    const attacks = {
      weapon: [],
      display: [],
    };

    const actions = {
      abilities: {
        label: game.i18n.localize('CONAN.npcActionTypes.abilities'),
        actions: [],
      },
      doom: {
        label: game.i18n.localize('CONAN.npcActionTypes.doom'),
        actions: [],
      },
    };

    const allItems = this._sortAllItems(context);

    // Get Attacks
    for (const i of allItems) {
      i.img = i.img || CONST.DEFAULT_TOKEN;

      if (i.type === 'npcattack') {
        let item;
        try {
          item = this.actor.getEmbeddedDocument('Item', i._id);
          i.chatData = item.getChatData({secrets: this.actor.isOwner});
        } catch (err) {
          console.error(
            `Conan 2D20 System | NPC Sheet | Could not load item ${i.name}`
          );
        }

        if (i.system.attackType === 'threaten') {
          attacks.display.push(i);
        } else {
          i.isShield = item.isShield();
          attacks.weapon.push(i);
        }
      } else if (i.type === 'npcaction') {
        const actionType = i.system.actionType || 'npcaction';
        actions[actionType].actions.push(i);
      }

      if (i.type !== 'npcattack' && i.type !== 'npcaction') {
        // Invalid Items
        console.error('Invalid item for non-player characters!');
        this.actor.deleteEmbeddedDocuments('Item', [i._id]);
      }
    }

    context.actions = actions;
    context.attacks = attacks;
  }

  _onRollSkillCheck(event) {
    event.preventDefault();

    const skill = $(event.currentTarget)
      .parents('.npc-skills__item')
      .attr('data-skill');

    this._rollSkillCheck(skill);
  }
}

export default ActorSheetConan2d20NPC;
