/**
 * Extend the base Actor class to implement additiona logic specialized for Conan2d20
 */
import Counter from '../system/counter';
import SkillRoller from '../apps/skill-roller';

export default class Conan2d20Actor extends Actor {
  /**
   *
   * Set initial actor data based on type
   *
   */
  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);

    const nameMode = game.settings.get('conan2d20', 'defaultTokenSettingsName');
    const barMode = game.settings.get('conan2d20', 'defaultTokenSettingsBar');

    const prototypeToken = {
      bar1: {attribute: 'health.physical'}, // Default Bar 1 to Wounds
      bar2: {attribute: 'health.mental'}, // Default Bar 1 to Wounds
      displayName: nameMode, // Default display name to be on owner hover
      displayBars: barMode, // Default display bars to be on owner hover
      disposition: CONST.TOKEN_DISPOSITIONS.FRIENDLY, // Default disposition to hostile
      name: data.name, // Set token name to actor name
    };

    if (data.type === 'character') {
      prototypeToken.sight = {
        enabled: true,
      };
      prototypeToken.actorLink = true;
    }

    if (data.type === 'npc') {
      prototypeToken.actorLink = false;
      prototypeToken.disposition = CONST.TOKEN_DISPOSITIONS.HOSTILE;
    }

    const update = {prototypeToken};
    if (!data.img) {
      if (data.type === 'character') {
        update.img =
          'systems/conan2d20/assets/tokens/conan-default-actor-green.webp';
      }
      else if (data.type === 'npc') {
        update.img =
          'systems/conan2d20/assets/tokens/conan-default-actor-red.webp';
      }

    }

    this.updateSource(update);
  }

  async applyDamage(damage) {
    const actorUpdate = {};

    if (this.system.isMob) {
      let mobUpdate = {};
      [damage, mobUpdate] = await this._applyMobDamage(damage);
      mergeObject(actorUpdate, mobUpdate);
    }

    if (damage.vigor > 0) {
      const vigorUpdates = await this._applyVigorDamage(damage);
      mergeObject(actorUpdate, vigorUpdates);
    }

    if (damage.resolve > 0) {
      const resolveUpdates = await this._applyResolveDamage(damage);
      mergeObject(actorUpdate, resolveUpdates);
    }

    await this.update(actorUpdate);

    if (this.isDead()) {
      this._setDefeated();

      ui.notifications.info(
        game.i18n.format(
          'CONAN.AppliedDamageToActorDefeated',
          {
            actorName: this.name,
          },
        )
      );
    }
    else {
      ui.notifications.info(
        game.i18n.format(
          'CONAN.AppliedDamageToActor',
          {
            actorName: this.name,
          },
        )
      );
    }
  }

  async bankMomentum() {
    const banked = this.system.momentum;

    const poolType = this.isNpc() ? 'doom' : 'momentum';

    if (banked > 0) {
      Counter.changeCounter(banked, poolType);
      this.update({'system.momentum': 0});

      let html = `<h2>${game.i18n.localize('CONAN.rollMomentumBanked')}</h2><div>`;

      html += `<p>${game.i18n.format('CONAN.momentumBankedChatText', {
        character: `<b>${this.name}</b>`,
        banked: `<b>${banked}</b>`,
        poolType: `<b>${poolType}</b>`
      })}</p></div>`;

      const chatData = {
        user: game.user.id,
        content: html,
      };

      ChatMessage.create(chatData);
    }
  }

  async getArmor(armorLocation) {
    if (this.isNpc()) {
      return this.system.armor;
    }
    else {
      return await this.sheet.getArmor(armorLocation);
    }
  }

  async getAvailableDoom() {
    return game.settings.get('conan2d20', 'doom');
  }

  async getAvailableFortune() {
    if (this.type === 'npc') {
      const doom = game.settings.get('conan2d20', 'doom');
      return Math.floor(doom / 3);
    } else {
      return this.system.resources.fortune.value;
    }
  }

  async getAvailableMomentum() {
    return this.system.momentum + game.settings.get('conan2d20', 'momentum');
  }

  getCourage() {
    return this.system.health.courage;
  }

  getCurrentResolve() {
    return this.system.health.mental.value;
  }

  getCurrentTraumas() {
    return this.system.health.mental.traumas.value;
  }

  getCurrentVigor() {
    return this.system.health.physical.value;
  }

  getCurrentWounds() {
    return this.system.health.physical.wounds.value;
  }

  getDifficultyIncrease(attribute) {
    const mentalSkill = ['int', 'per', 'wil'].includes(attribute);

    if (mentalSkill) {
      return this.getCurrentTraumas();
    }
    else {
      return this.getCurrentWounds();
    }
  }

  // return an item owned by the user with the specified name, or null.
  getItemByName(itemName) {
    return this.items.find(item => item.name === itemName) || null;
  }

  getKits() {
    return this.collections.items.filter(entry => entry.type === 'kit');
  }

  getMaxResolve() {
    return (
      this.system.attributes.wil.value +
      this.system.skills.dis.expertise.value -
      this.system.health.mental.despair +
      this.system.health.mental.bonus
    );
  }

  getMaxTraumas() {
    return this.system.health.mental.traumas.max;
  }

  getMaxWounds() {
    return this.system.health.physical.wounds.max;
  }

  getMaxVigor() {
    return (
      this.system.attributes.bra.value +
      this.system.skills.res.expertise.value -
      this.system.health.physical.fatigue +
      this.system.health.physical.bonus
    );
  }

  getMergedReloads() {
    const reloads = this.getReloads();

    const availableReloads = {};

    for (let reload of reloads) {
      if (!availableReloads[reload.name]) {
        availableReloads[reload.name] = {
          name: reload.name,
          uses: parseInt(reload.uses),
          max: parseInt(reload.max),
          ids: [reload.id],
        };
      } else {
        availableReloads[reload.name].uses += parseInt(reload.uses);
        availableReloads[reload.name].max += parseInt(reload.max);
        availableReloads[reload.name].ids.push(reload.id);
      }
    }

    let mergedReloads = [];
    /* eslint-disable-next-line no-unused-vars */
    for (const [key, value] of Object.entries(availableReloads)) {
      mergedReloads.push(availableReloads[key]);
    }

    return mergedReloads;
  }

  getReloads() {
    return this.items
      .filter(i => i.system.kitType === 'reload')
      .map(
        i =>
          ({
            id: i.id,
            name: i.name,
            uses: i.system.uses.value,
            max: i.system.uses.max,
          } || [])
      )
      .sort((a, b) => {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      });
  }

  getSkillTargetNumberAndFocus(attribute, skillKey, expertiseKey) {
    const actorSkills = this.system.skills;

    let skillTn = parseInt(this.system.attributes[attribute].value) || 0;
    let skillFocus = 0;
    let skillExpertise = 0;

    if (this.type === 'npc') {
      skillFocus = skillExpertise =
        parseInt(actorSkills[expertiseKey].value) || 0;
      skillTn += skillExpertise;
    } else {
      skillExpertise = parseInt(actorSkills[skillKey].expertise.value) || 0;
      skillFocus = parseInt(actorSkills[skillKey].focus.value) || 0;
      skillTn += skillExpertise;
    }

    return [skillTn, skillExpertise, skillFocus];
  }

  getTreatedTraumas() {
    let treatedTraumas = 0;
    if (this.isCharacter()) {
      for (let i = 0; i < 5; i++) {
        const dot = this.system.health.mental.traumas.dots[i]
        if (dot.status === "treated") treatedTraumas++;
      }
    }
    return treatedTraumas;
  }

  getTreatedWounds() {
    let treatedWounds = 0;
    if (this.isCharacter()) {
      for (let i = 0; i < 5; i++) {
        const dot = this.system.health.physical.wounds.dots[i]
        if (dot.status === "treated") treatedWounds++;
      }
    }
    return treatedWounds;
  }

  /**
   * Augment the basic actor data with additional dynamic data
   */
  prepareData() {
    super.prepareData();

    // Get the Actor's data object
    const actorData = this.system;
    // const {data} = actorData;

    // Prepare Character data
    if (this.type === 'character') {
      this._prepareCharacterData(actorData);
    } else if (this.type === 'npc') {
      this._prepareNPCData(actorData);
    }

    if (actorData.qualities !== undefined) {
      const map = {};
      for (const [t] of Object.entries(map)) {
        const quality = actorData.qualities[t];
        if (quality === undefined) {
          /* eslint-disable-next-line no-continue */
          continue;
        }
      }
    }

    // Return the prepared Actor data
    return actorData;
  }

  async _applyMobDamage(damage) {
    const mobMembers = duplicate(this.system.grouping.members);

    if (damage.vigor > 0) {
      for (const member of mobMembers) {
        if (this.isMobMemberDead(member)) continue;
        if (damage.vigor <= 0) break;

        damage.vigor -= member.armor;

        let newWounds = 0;

        if (damage.vigor >= 5) newWounds++;

        if (damage.vigor >= member.vigor.value) {
          damage.vigor -= member.vigor.value;
          member.vigor.value = 0;
          newWounds++;
        }
        else {
          member.vigor.value -= damage.vigor;
          damage.vigor = 0;
        }

        if (newWounds > 0) {
          for (let i = 0; i < member.vigor.wounds.length; i++) {
            if (newWounds <= 0) break;
            if (!member.vigor.wounds[i].active) {
              member.vigor.wounds[i].active = true;
              newWounds--;
            }
          }
        }

        if (this.isMobMemberDead(member)) member.dead = true;
      }
    }

    if (damage.resolve > 0) {
      for (const member of mobMembers) {
        if (this.isMobMemberDead(member)) continue;
        if (damage.resolve <= 0) break;
        damage.resolve -= member.courage;

        let newTraumas = 0;

        if (damage.resolve >= 5) newTraumas++;

        if (damage.resolve >= member.resolve.value) {
          damage.resolve -= member.resolve.value;
          member.resolve.value = 0;
          newTraumas++;
        }
        else {
          member.resolve.value -= damage.resolve;
          damage.resolve = 0;
        }

        if (newTraumas > 0) {
          for (let i = 0; i < member.resolve.traumas.length; i++) {
            if (newTraumas <= 0) break;
            if (!member.resolve.traumas[i].active) {
              member.resolve.traumas[i].active = true;
              newTraumas--;
            }
          }
        }

        if (this.isMobMemberDead(member)) member.dead = true;
      }
    }

    return [damage, {"system.grouping.members": mobMembers}];
  }

  async _applyResolveDamage(damage) {
    const actorUpdate = {};

    const courage = await this.getCourage();

    const maxTraumas = this.getMaxTraumas();
    let resolveRemaining = this.getCurrentResolve();
    let currentTraumas = this.getCurrentTraumas();
    let treatedTraumas = this.getTreatedTraumas();

    // Reduce the damage by the amount of armor soak
    let damageRemaining = damage.resolve - courage;

    if (damageRemaining <= 0) return actorUpdate;

    // five or more damage at once causes a wound automatically
    let newWounds = damageRemaining >= 5 ? 1 : 0;

    if (resolveRemaining > 0) {
      resolveRemaining -= damageRemaining;

      // If Vigor drops to zero or below then a wound is caused
      if (resolveRemaining <= 0) {
        resolveRemaining = 0;
        newWounds++;
      }
    }
    else {
        // Damage when at zero Vigor always causes a wound
        newWounds++;
    }

    if (newWounds > 0) {
      currentTraumas += treatedTraumas + newWounds;

      if (this.isCharacter()) {
        let woundsToAdd = newWounds;
        const woundDots = duplicate(this.system.health.mental.traumas.dots);
        for (let i = 0; i < 5; i++) {
          switch (woundDots[i].status) {
            case "healed":
              if (woundsToAdd <= 0) continue;

              woundDots[i].status = "wounded";
              woundDots[i].icon = "fas fa-skull";
              woundsToAdd--;
              break;
            case "treated":
              // Having a new wound causes any treated ones to re-open
              woundDots[i].status = "wounded";
              woundDots[i].icon = "fas fa-skull";
              break;
          }
        }

        actorUpdate["system.health.mental.traumas.dots"] = woundDots;
      }
      else {
        currentTraumas = currentTraumas > maxTraumas ? maxTraumas : currentTraumas;
      }
    }

    actorUpdate["system.health.mental.value"] = resolveRemaining;
    actorUpdate["system.health.mental.traumas.value"] = currentTraumas;

    return actorUpdate;
  }

  async _applyVigorDamage(damage) {
    const actorUpdate = {};

    const armor = await this.getArmor(damage.location);

    // Armor values are stored differently depending on whether the Actor is
    // an NPC or Character
    //
    let soak = 0;
    if (this.isCharacter()) {
      soak = armor.soak[0] ?? 0;
    }
    else {
      soak = armor;
    }

    const maxWounds = this.getMaxWounds();
    let vigorRemaining = this.getCurrentVigor();
    let currentWounds = this.getCurrentWounds();
    let treatedWounds = this.getTreatedWounds();

    // Reduce the damage by the amount of armor soak
    let damageRemaining = damage.vigor - soak;

    if (damageRemaining <= 0) return actorUpdate;

    // five or more damage at once causes a wound automatically
    let newWounds = damageRemaining >= 5 ? 1 : 0;

    if (vigorRemaining > 0) {
      vigorRemaining -= damageRemaining;

      // If Vigor drops to zero or below then a wound is caused
      if (vigorRemaining <= 0) {
        vigorRemaining = 0;
        newWounds++;
      }
    }
    else {
        // Damage when at zero Vigor always causes a wound
        newWounds++;
    }

    if (newWounds > 0) {
      currentWounds += treatedWounds + newWounds;

      if (this.isCharacter()) {
        let woundsToAdd = newWounds;
        const woundDots = duplicate(this.system.health.physical.wounds.dots);
        for (let i = 0; i < 5; i++) {
          switch (woundDots[i].status) {
            case "healed":
              if (woundsToAdd <= 0) continue;

              woundDots[i].status = "wounded";
              woundDots[i].icon = "fas fa-skull";
              woundsToAdd--;
              break;
            case "treated":
              // Having a new wound causes any treated ones to re-open
              woundDots[i].status = "wounded";
              woundDots[i].icon = "fas fa-skull";
              break;
          }
        }

        actorUpdate["system.health.physical.wounds.dots"] = woundDots;
      }
      else {
        currentWounds = currentWounds > maxWounds ? maxWounds : currentWounds;
      }
    }

    actorUpdate["system.health.physical.value"] = vigorRemaining;
    actorUpdate["system.health.physical.wounds.value"] = currentWounds;

    return actorUpdate;
  }

  /* -------------------------------------------- */

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    // Calculate Vigor
    if (
      isNaN(actorData.health.physical.bonus) ||
      actorData.health.physical.bonus === null
    ) {
      actorData.health.physical.bonus = 0;
    }

    actorData.health.physical.max =
      actorData.attributes.bra.value +
      actorData.skills.res.expertise.value -
      actorData.health.physical.fatigue +
      actorData.health.physical.bonus;

    if (actorData.health.physical.value === null) {
      actorData.health.physical.value =
        actorData.attributes.bra.value + actorData.skills.res.expertise.value;
    } else if (
      actorData.health.physical.value > actorData.health.physical.max
    ) {
      actorData.health.physical.value = actorData.health.physical.max;
    } else if (actorData.health.physical.value < 0) {
      actorData.health.physical.value = 0;
    }

    // Calculate Resolve
    if (
      isNaN(actorData.health.mental.bonus) ||
      actorData.health.mental.bonus === null
    ) {
      actorData.health.mental.bonus = 0;
    }

    actorData.health.mental.max =
      actorData.attributes.wil.value +
      actorData.skills.dis.expertise.value -
      actorData.health.mental.despair +
      actorData.health.mental.bonus;

    if (actorData.health.mental.value === null) {
      actorData.health.mental.value =
        actorData.attributes.wil.value + actorData.skills.dis.expertise.value;
    } else if (actorData.health.mental.value > actorData.health.mental.max) {
      actorData.health.mental.value = actorData.health.mental.max;
    } else if (actorData.health.mental.value < 0) {
      actorData.health.mental.value = 0;
    }

    // Set TN for Skills
    for (const [s, skl] of Object.entries(actorData.skills)) {
      skl.tn.value =
        skl.expertise.value + actorData.attributes[skl.attribute].value;
      if (actorData.skills[s].expertise.value > 0) {
        actorData.skills[s].trained = true;
      }
    }

    // Prepare Upkeep Cost
    actorData.resources.upkeep.value =
      3 + actorData.background.standing.value - actorData.background.renown;
    if (actorData.resources.upkeep.value < 0) {
      actorData.resources.upkeep.value = 0;
    }

    // Automatic Actions
    actorData.actions = [];

    // Sync Wounds & Traumas from Dots
    let wounds = 0;
    let traumas = 0;
    for (let i = 0; i < 5; i++) {
      const woundDot = actorData.health.physical.wounds.dots[i]
      if (woundDot.status === "wounded") wounds++;
      const traumaDot = actorData.health.mental.traumas.dots[i]
      if (traumaDot.status === "wounded") traumas++;
    }
    actorData.health.physical.wounds.value = wounds;
    actorData.health.mental.traumas.value = traumas;
  }

  /* -------------------------------------------- */

  /**
   * Prepare Character type specific data
   */
  _prepareNPCData(actorData) {
    let maxWounds = 1; // default minion maxWounds

    if (actorData.type === 'toughened') {
      maxWounds = 2;
    } else if (actorData.type === 'nemesis') {
      maxWounds = 5;
    }

    actorData.health.mental.traumas.max = maxWounds;
    actorData.health.physical.wounds.max = maxWounds;

    if (actorData.isMob) {
      let mobMembersAlive = actorData.grouping.members.length;
      for (const member of actorData.grouping.members) {
        if (this.isMobMemberDead(member)) {
          mobMembersAlive--;
        }
      }

      if (this.isAlive()) {
        mobMembersAlive++;
      }

      actorData.mobMembersAlive = {
        value: mobMembersAlive,
        max: CONFIG.CONAN.DEFAULT_MOB_SIZE,
      };
    }
    else {
      actorData.mobMembersAlive = {
        value: 0,
        max: 0,
      };
    }
  }

  static addDoom(doomSpend) {
    Counter.changeCounter(+`${doomSpend}`, 'doom');
  }

  static addMomentum(momentumSpend) {
    Counter.changeCounter(+`${momentumSpend}`, 'momentum');
  }

  static payDoom(actorData, doomSpend) {
    if (!doomSpend > 0) return;

    const actor = game.actors.get(actorData._id);

    Counter.changeCounter(+`${doomSpend}`, 'doom');

    let html = `<h2>${game.i18n.localize('CONAN.rollDoomPaid')}</h2><div>`;

    html += `<p>${game.i18n.format('CONAN.rollDoomPaidChatText', {
      character: `<b>${actor.name}</b>`,
      spent: `<b>${doomSpend}</b>`,
    })}</p></div>`;

    const chatData = {
      user: game.user.id,
      content: html,
    };

    ChatMessage.create(chatData);
  }

  static spendFortune(actorData, fortuneSpend) {
    if (!fortuneSpend > 0) return;

    const newValue = actorData.system.resources.fortune.value - fortuneSpend;

    if (newValue < 0) {
      const error = 'Fortune spend would exceed available fortune points.';
      throw error;
    } else {
      const updateActorData = {
        'system.resources.fortune.value': newValue,
      };
      game.actors.get(actorData._id).update(updateActorData);
    }
  }

  static buyFortune(actor, numFortune) {
    if (!numFortune > 0) return;
    if (!actor.type === 'npc') return;

    const doomCost = numFortune * 3;
    const newValue = game.settings.get('conan2d20', 'doom') - doomCost;

    if (newValue < 0) {
      const error = 'Doom cost of Fortune would exceed available Doom points.';
      throw error;
    } else {
      Counter.changeCounter(-`${doomCost}`, 'doom');
    }

    let html = `<h2>${game.i18n.localize('CONAN.rollFortuneBought')}</h2><div>`;

    html += `<p>${game.i18n.format('CONAN.rollFortuneBoughtChatText', {
      character: `<b>${actor.name}</b>`,
      spent: `<b>${doomCost}</b>`,
      fortune: `<b>${numFortune}</b>`,
    })}</p></div>`;

    const chatData = {
      user: game.user.id,
      content: html,
    };

    ChatMessage.create(chatData);
  }

  static spendDoom(actor, doomSpend) {
    if (!doomSpend > 0) return;

    const newValue = game.settings.get('conan2d20', 'doom') - doomSpend;

    if (newValue < 0) {
      const error = 'Doom spend would exceed available doom points.';
      throw error;
    } else {
      Counter.changeCounter(-`${doomSpend}`, 'doom');
    }

    let html = `<h2>${game.i18n.localize('CONAN.rollDoomSpent')}</h2><div>`;

    html += `<p>${game.i18n.format('CONAN.rollDoomSpentChatText', {
      character: `<b>${actor.name}</b>`,
      spent: `<b>${doomSpend}</b>`,
    })}</p></div>`;

    const chatData = {
      user: game.user.id,
      content: html,
    };

    ChatMessage.create(chatData);
  }

  static spendMomentum(actorData, momentumSpend) {
    if (!momentumSpend > 0) return;

    const actor = game.actors.get(actorData._id);

    let playerMomentum = actor.system.momentum;
    let poolMomentum = game.settings.get('conan2d20', 'momentum');

    const availableMomentum = poolMomentum + playerMomentum;

    if (momentumSpend > availableMomentum) {
      const error = 'Momentum spend would exceed available momentum points.';
      throw error;
    } else {
      let newPoolMomentum = poolMomentum;

      playerMomentum -= momentumSpend;

      if (playerMomentum < 0) {
        newPoolMomentum += playerMomentum;
        playerMomentum = 0;
      }

      actor.update({'system.momentum': playerMomentum});
      Counter.setCounter(`${newPoolMomentum}`, 'momentum');

      let html = `<h2>${game.i18n.localize(
        'CONAN.rollMomentumSpent'
      )}</h2><div>`;

      html += `<p>${game.i18n.format('CONAN.rollMomentumSpentChatText', {
        character: `<b>${actor.name}</b>`,
        spent: `<b>${momentumSpend}</b>`,
      })}</p></div>`;

      const chatData = {
        user: game.user.id,
        content: html,
      };

      ChatMessage.create(chatData);
    }
  }

  // static spendReload(actor, reloadSpend, reloadItemId) {
  //   const reloadItem = actor.items.find(i => i._id === reloadItemId);

  //   const newValue = reloadItem.system.uses.value - reloadSpend;

  //   if (newValue < 0) {
  //     const error = 'Resource spend would exceed available reloads.';
  //     throw error;
  //   } else {
  //     reloadItem.system.uses.value = newValue;
  //     actor.update();
  //   }
  // }

  async spendReloads(reload, quantity) {
    while (quantity > 0 && reload.ids.length > 0) {
      const id = reload.ids.pop();
      const reloadItem = this.getEmbeddedDocument('Item', id);

      const remaining = reloadItem.system.uses.value;

      let useCount = 0;
      if (remaining > 0) {
        if (quantity >= remaining) {
          useCount = remaining;
          quantity -= remaining;
        } else {
          useCount = quantity;
        }

        this.updateEmbeddedDocuments('Item', [
          {
            _id: id,
            'system.uses.value': remaining - useCount,
          },
        ]);
      }
    }
  }

  _getModifiers(type, specifier) {
    let mod;
    if (type === 'skill') {
      const difficultyLevels = CONFIG.rollDifficultyLevels;
      const diceModSpends = CONFIG.skillRollResourceSpends;
      const prefilledDifficulty = this._getPrefilledDifficulty(specifier.skill);
      const prefilledAttribute = this._getPrefilledAttribute(specifier.skill);
      if (this.type === 'npc') {
        mod = {
          difficulty: difficultyLevels,
          prefilledDifficulty: prefilledDifficulty.difficulty,
          prefilledAttribute: prefilledAttribute.attribute,
          diceModifier: diceModSpends,
          successModifier: 0,
          npcAttributes: CONFIG.attributes,
          actorType: this.type,
        };
        return mod;
      }
      mod = {
        difficulty: difficultyLevels,
        prefilledDifficulty: prefilledDifficulty.difficulty,
        difficultyTooltip: prefilledDifficulty.tooltipText,
        diceModifier: diceModSpends,
        successModifier: 0,
        actorType: this.type,
      };
      return mod;
    }
    if (type === 'damage') {
      const attackTypes = CONFIG.weaponTypes;

      let wType = '';
      let attacker = 'character'; // default to character
      let attackBonuses = this._attackBonuses();

      if (specifier.type === 'display') {
        wType = 'display';
      } else if (specifier.type === 'weapon') {
        wType = specifier.system.weaponType;
      } else if (specifier.type === 'npcattack') {
        attacker = 'npc';
        wType = specifier.system.attackType;
      }

      mod = {
        attacker,
        attackTypes,
        baseDamage: specifier.system.damage.dice,
        weaponType: wType,
        momentumModifier: 0,
        reloadModifier: 0,
        talentModifier: 0,
        attackBonuses,
      };
    }
    return mod;
  }

  /**
   * Uses selected field of expertise to determine which attribute the test
   * should use by default.
   * @param expertise Expertise being rolled
   */
  _getPrefilledAttribute(expertise) {
    return {attribute: CONFIG.expertiseAttributeMap[expertise]};
  }

  /**
   * Uses contextual data like conditions or harms to determine how much to
   * increase test difficulty by
   * @param skill Skill being used
   */
  _getPrefilledDifficulty(skill) {
    const tooltip = [];
    let difficulty = 1;
    if (this.hasCondition('dazed')) {
      difficulty += 1;
      tooltip.push({name: CONFIG.conditionTypes.dazed, value: 1});
    }

    // Open to change here, as what is affected and what isn't is up in the air
    const blindedAffected = [
      'obs',
      'ins',
      'ran',
      'mel',
      'sai',
      'par',
      'cmb',
      'mov',
      'sns',
    ];
    if (this.hasCondition('blind') && blindedAffected.includes(skill)) {
      difficulty += 2;
      tooltip.push({name: CONFIG.conditionTypes.blind, value: 2});
    }

    const deafAffected = ['obs', 'ins', 'com', 'per', 'sns'];
    if (this.hasCondition('deaf') && deafAffected.includes(skill)) {
      difficulty += 2;
      tooltip.push({name: CONFIG.conditionTypes.deaf, value: 2});
    }

    if (this.actorType === 'character') {
      const physicalTests = ['agi', 'bra', 'coo'];
      const mentalTests = ['awa', 'int', 'per', 'wil'];

      const wounds = Object.values(
        this.system.health.physical.wounds.dots
      ).reduce((acc, w) => {
        acc += w.status === 'wounded' ? 1 : 0;
        return acc;
      }, 0);

      if (
        wounds > 0 &&
        physicalTests.includes(CONFIG.skillAttributeMap[skill])
      ) {
        difficulty += wounds;
        tooltip.push({name: 'Wounds', value: wounds});
      }

      const trauma = Object.values(
        this.system.health.mental.traumas.dots
      ).reduce((acc, w) => {
        acc += w.status === 'wounded' ? 1 : 0;
        return acc;
      }, 0);
      if (trauma > 0 && mentalTests.includes(CONFIG.skillAttributeMap[skill])) {
        difficulty += trauma;
        tooltip.push({name: 'Trauma', value: trauma});
      }
    } else if (this.actorType === 'npc') {
      const physicalTests = ['agi', 'bra'];
      const mentalTests = ['per', 'int', 'awa'];
      const wounds = this.system.health.physical.wounds.value;
      const traumas = this.system.health.mental.traumas.value;

      if (
        wounds > 0 &&
        physicalTests.includes(CONFIG.expertiseAttributeMap[skill])
      ) {
        difficulty += wounds;
        tooltip.push({name: 'Wounds', value: wounds});
      }
      if (
        traumas > 0 &&
        mentalTests.includes(CONFIG.expertiseAttributeMap[skill])
      ) {
        difficulty += traumas;
        tooltip.push({name: 'Traumas', value: traumas});
      }
    }

    difficulty += this.system.difficultyModifier || 0;

    if (difficulty > 5) difficulty = 5;

    let tooltipText = '';
    if (tooltip.length)
      tooltipText = tooltip.map(i => `${i.name}: +${i.value}`).join('\n');

    return {difficulty, tooltipText};
  }

  _attackBonuses() {
    const isNpc = this.isNpc();

    return {
      threaten: isNpc ? 0 : this._attributeBonus('per'),
      melee: isNpc ? 0 : this._attributeBonus('bra'),
      ranged: isNpc ? 0 : this._attributeBonus('awa'),
    };
  }

  _attributeBonus(attribute) {
    const attributeValue = this.system.attributes[attribute].value;

    if (attributeValue <= 8) return 0;
    if (attributeValue <= 9) return 1;
    if (attributeValue <= 11) return 2;
    if (attributeValue <= 13) return 3;
    if (attributeValue <= 15) return 4;
    if (attributeValue >= 16) return 5;
  }

  async _rollSkillCheck(skill, item = null, bonusDice) {
    const isNpc = this.type === 'npc';

    const attribute = isNpc
      ? CONFIG.expertiseAttributeMap[skill]
      : CONFIG.skillAttributeMap[skill];

    const skillData = {
      attribute,
      bonusDice,
      skill: isNpc ? null : skill,
      expertise: isNpc ? skill : null,
      item,
    };

    new SkillRoller(this, skillData).render(true);
  }

  async _setDefeated() {
    // If this actor is in a combat tracker, then set them as defeated and
    // add the "dead" effect icon to the token in the scene
    //
    for (const combat of game.combats) {
      for (const combatant of combat.combatants) {

        // Make sure we're matching on the correct id depending on whether we're
        // a linked actor, or just a token
        //
        const matchFound = this.isToken
          ? combatant.tokenId === this.token.id
          : combatant.actorId === this.id;

        if (!matchFound) continue;

        combatant.update({defeated: true});

        const token = combatant.token;
        if (!token) return;

        // Push the defeated status to the token
        const statusEffect = CONFIG.statusEffects.find(
          e => e.id === CONFIG.specialStatusEffects.DEFEATED
        );

        if (!statusEffect && !token.object) return;

        const effect = token.actor && statusEffect
          ? statusEffect
          : CONFIG.controlIcons.defeated;

        if (token.object) {
          await token.object.toggleEffect(
            effect,
            {
              overlay: true,
              active: true,
            }
          );
        }
        else {
          await token.toggleActiveEffect(
            effect,
            {
              overlay: true,
              active: true,
            }
          );
        }
      }
    }
  }

  async getRollOptions(rollNames) {
    const flag = this.getFlag(game.system.id, 'rollOptions') ?? {};
    return rollNames
      .flatMap(rollName =>
        // convert flag object to array containing the names of all fields with a truthy value
        Object.entries(flag[rollName] ?? {}).reduce(
          (opts, [key, value]) => opts.concat(value ? key : []),
          []
        )
      )
      .reduce((unique, option) => {
        // ensure option entries are unique
        return unique.includes(option) ? unique : unique.concat(option);
      }, []);
  }

  async addCondition(effect, value = 1) {
    if (typeof effect === 'string')
      effect = duplicate(
        game.conan2d20.config.statusEffects.find(e => e.id === effect)
      );
    if (!effect) return 'No Effect Found';

    if (!effect.id) return 'Conditions require an id field';

    effect.label = game.i18n.localize(effect.label);

    // eslint-disable-next-line prefer-const
    let existing = this.hasCondition(effect.id);
    if (existing && existing.flags.conan2d20.value === null) {
      return existing;
    }

    if (existing) {
      existing = duplicate(existing);
      existing.flags.conan2d20.value += value;
      return this.updateEmbeddedDocuments('ActiveEffect', [existing]);
    }

    if (!existing) {
      if (Number.isNumeric(effect.flags.conan2d20.value)) {
        effect.flags.conan2d20.value = value;
      }

      if (game.conan2d20.isV11) {
        const statuses = effect.statuses ?? [];
        statuses.push(effect.id);

        effect.statuses = statuses;
      }
      else {
        // TODO Remove once we have v11 support minimum
        effect['flags.core.statusId'] = effect.id;
      }
    }
    return this.createEmbeddedDocuments('ActiveEffect', [effect]);
  }

  isAlive() {
    return !this.isDead();
  }

  isDead() {
    const wounds = this.system.health.physical.wounds;
    const woundsFull = wounds.value >= wounds.max;

    const traumas = this.system.health.mental.traumas;
    const traumasFull = traumas.value >= traumas.max;

    return woundsFull || traumasFull;
  }

  isMobMemberDead(mobMemberData) {
    let woundsFull = true;
    for (const wound of mobMemberData.vigor.wounds) {
      if (!wound.active) {
        woundsFull = false;
        break;
      }
    }

    let traumasFull = true;
    for (const trauma of mobMemberData.resolve.traumas) {
      if (!trauma.active) {
        traumasFull = false;
        break;
      }
    }

    return woundsFull || traumasFull;
  }

  isNpc() {
    return this.type === 'npc';
  }

  isCharacter() {
    return this.type === 'character';
  }

  async removeCondition(effect, value = 1) {
    effect = duplicate(
      game.conan2d20.config.statusEffects.find(e => e.id === effect)
    );
    if (!effect) {
      return 'No Effect Found';
    }
    if (!effect.id) {
      return 'Conditions require an id field';
    }

    // eslint-disable-next-line prefer-const
    let existing = this.hasCondition(effect.id);
    if (existing) {
      const duplicated = duplicate(existing);
      duplicated.flags.conan2d20.value -= value;

      if (duplicated.flags.conan2d20.value <= 0) {
        return this.deleteEmbeddedDocuments('ActiveEffect', [existing.id]);
      }

      return this.updateEmbeddedDocuments('ActiveEffect', [duplicated]);
    }
  }

  hasCondition(conditionKey) {
    let existing;
    if (game.conan2d20.isV11) {
      existing = this.effects.find(
        i => i.statuses.has(conditionKey)
      );
    }
    else {
      existing = this.effects.find(
        i => i.flags.core.statusId === conditionKey
      );
    }

    return existing;
  }

  // Return the type of the current actor
  get actorType() {
    return this.type;
  }

  async useKit(itemId) {
    const item = this.getEmbeddedDocument('Item', itemId);

    const totalUses = item.system.uses.value;
    const availableUses = totalUses > 3 ? 3 : totalUses;

    if (availableUses <= 0) {
      return ui.notifications.warn(
        game.i18n.format('CONAN.KitHasNoUsesAvailable', {kitName: item.name})
      );
    }

    renderTemplate(
      'systems/conan2d20/templates/dialog/use-kit-dialog.html',
      {
        availableUses,
        totalUses,
        kitName: item.name,
      }
    ).then(html => {
      new Dialog({
        title: 'Confirm Kit Use',
        content: html,
        buttons: {
          Yes: {
            icon: '<i class="fa fa-check"></i>',
            label: 'Yes',
            callback: async (formData) => {
              const input = $(formData).find('.chosen-uses')[0];
              const uses = Number.parseInt(input.value);
              if (Number.isInteger(uses)) {
                if (uses > 0 && uses <= availableUses) {
                  const remainingUses = item.system.uses.value - uses;

                  item.update({'system.uses.value': remainingUses});

                  let chatHtml = `<h2>${game.i18n.localize('CONAN.kitChargesUsed')}</h2><div>`;

                  chatHtml += `<p>${game.i18n.format('CONAN.usedKitChargesChatText', {
                    actorName: `<b>${this.name}</b>`,
                    itemName: `<b>${item.name}</b>`,
                    uses: `<b>${uses}</b>`,
                  })}</p></div>`;

                  const chatData = {
                    user: game.user.id,
                    content: chatHtml,
                  };

                  ChatMessage.create(chatData);

                  this._rollSkillCheck(item.system.skill, item, uses)
                  console.log(`Chosen uses: ${uses}`);
                }
                else {
                  return ui.notifications.error(`Bad Use Value: ${uses} (${availableUses} available)`);
                }
              }
              else {
                return ui.notifications.error(`Bad Use Value: ${uses} (${availableUses} available)`);
              }
            },
          },
          Cancel: {
            icon: '<i class="fa fa-times"></i>',
            label: 'Cancel',
          },
        },
        default: 'Yes',
      }).render(true);
    });
  }
}
