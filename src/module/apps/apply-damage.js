export default class ConanApplyDamage extends Application {

  constructor(options) {
    super(options);

    this.actors = options.actors.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });

    this.damage = {
      resolve: 0,
      vigor: 0,
      location: 'head',
    };

  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ['conan2d20', 'apply-damage'],
      template: 'systems/conan2d20/templates/apps/apply-damage.html',
      width: 280,
      height: 'auto',
      submitOnChange: false,
    });
  }

  get title() {
    return `${game.i18n.localize('CONAN.ApplyDamage')}`;
  }

  activateListeners(html) {
    const me = this;

    html.find('.apply-damage-button').click(async event => {
      event.preventDefault();
      this.applyDamage();
      this.close();
    });

    html.find('.character').click(async event => {
      event.preventDefault();
      const index = $(event.currentTarget).data("actor-index");
			const actor = this.actors[index];
      actor.sheet.render(true);
    });

    html.find('.character').contextmenu(async event => {
      event.preventDefault();
      const index = $(event.currentTarget).data("actor-index");
      const newActorList = [];

      for (let i = 0; i < this.actors.length; i++) {
        if (index === i) continue;
        newActorList.push(this.actors[i]);
      }

      if (newActorList.length > 0) {
        this.actors = newActorList;
        this.render(false);
      }
      else {
        this.close();
      }
    });

    html.find('.apply-damage .quantity-grid').each(function () {
      const spinner = $(this);
      const input = spinner.find('input[type="number"]');
      const btnUp = spinner.find('.quantity-up');
      const btnDown = spinner.find('.quantity-down');
      const quantityType = input.attr('data-quantity-type');

      input.on('wheel', ev => {
        ev.preventDefault();
        if (ev.originalEvent.deltaY < 0) {
          me.damage[quantityType] += 1;
        } else if (ev.originalEvent.deltaY > 0) {
          me.damage[quantityType] -= 1;
          me.damage[quantityType] = me.damage[quantityType] >= 0
            ? me.damage[quantityType]
            : 0;
        }
        me.render(false);
      });

      btnUp.click(ev => {
        ev.preventDefault();
        me.damage[quantityType] += 1;
        me.render(false);
      });

      btnDown.click(ev => {
        ev.preventDefault();
        me.damage[quantityType] -= 1;
        me.damage[quantityType] = me.damage[quantityType] >= 0
          ? me.damage[quantityType]
          : 0;
        me.render(false);
      });
    });

    html.find('.apply-damage select').on('change', function () {
      const selector = $(this);
      const value = selector.val();
      me.damage.location = value;
    });

    super.activateListeners(html);
  }

  async applyDamage() {
    for (const actor of this.actors) {
      const damage = duplicate(this.damage);
      actor.applyDamage(damage);
    }
  }

  async getData() {
    const context = {
      damage: this.damage,
      selectedActors: this.actors,
      locations: CONFIG.CONAN.coverageTypes,
    }

    return context;
  }
}
